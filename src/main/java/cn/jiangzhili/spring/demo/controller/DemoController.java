package cn.jiangzhili.spring.demo.controller;

import cn.jiangzhili.spring.demo.dao.one.One;
import cn.jiangzhili.spring.demo.dao.two.Two;
import cn.jiangzhili.spring.demo.service.OneDataSourceService;
import cn.jiangzhili.spring.demo.service.TwoDataSourceService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author jiangzhili
 * @date 2022/9/18
 * @since 1.0.0
 */
@RestController
public class DemoController {
    @Resource
    private OneDataSourceService oneDataSourceService;

    @Resource
    private TwoDataSourceService twoDataSourceService;

    @ResponseBody
    @GetMapping("/getOneDataSource")
    public Object getOneDataSource(){
        return oneDataSourceService.lambdaQuery().list();
    }

    @ResponseBody
    @GetMapping("/getTwoDataSource")
    public Object getTwoDataSource(){
        return twoDataSourceService.lambdaQuery().list();
    }

    @ResponseBody
    @PostMapping("/saveOneDataSource")
    @Transactional(rollbackFor = Exception.class, value = "oneTransactionManager")
    public Object saveOneDataSource(){
        One one = new One();
        one.setTest("test");
        return oneDataSourceService.save(one);
    }

    @ResponseBody
    @PostMapping("/saveTwoDataSource")
    @Transactional(rollbackFor = Exception.class)
    public Object saveTwoDataSource(){
        Two two = new Two();
        two.setTest("test");
        return twoDataSourceService.save(two);
    }


}
