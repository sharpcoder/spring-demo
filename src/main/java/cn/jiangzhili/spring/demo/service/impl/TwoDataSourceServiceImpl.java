package cn.jiangzhili.spring.demo.service.impl;

import cn.jiangzhili.spring.demo.dao.two.Two;
import cn.jiangzhili.spring.demo.dao.two.TwoMapper;
import cn.jiangzhili.spring.demo.service.TwoDataSourceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author jiangzhili
 * @date 2022/9/18
 * @since 1.0.0
 */
@Service
public class TwoDataSourceServiceImpl extends ServiceImpl<TwoMapper, Two> implements TwoDataSourceService {
}
