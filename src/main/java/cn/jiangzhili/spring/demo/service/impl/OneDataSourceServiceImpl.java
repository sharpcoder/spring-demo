package cn.jiangzhili.spring.demo.service.impl;

import cn.jiangzhili.spring.demo.dao.one.One;
import cn.jiangzhili.spring.demo.dao.one.OneMapper;
import cn.jiangzhili.spring.demo.service.OneDataSourceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author jiangzhili
 * @date 2022/9/18
 * @since 1.0.0
 */
@Service
public class OneDataSourceServiceImpl extends ServiceImpl<OneMapper, One> implements OneDataSourceService {
}
