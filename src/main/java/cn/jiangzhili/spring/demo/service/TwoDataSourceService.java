package cn.jiangzhili.spring.demo.service;

import cn.jiangzhili.spring.demo.dao.two.Two;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author jiangzhili
 * @date 2022/9/18
 * @since 1.0.0
 */
public interface TwoDataSourceService extends IService<Two> {
}
