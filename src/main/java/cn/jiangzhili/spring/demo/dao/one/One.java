package cn.jiangzhili.spring.demo.dao.one;

import lombok.Data;

/**
 * 
 * @since 1.0.0
 * @author jiangzhili
 * @date 2022/9/18
 */
@Data
public class One {
    private Integer id;

    private String test;
}