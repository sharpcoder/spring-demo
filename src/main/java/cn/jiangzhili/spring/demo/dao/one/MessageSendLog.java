package cn.jiangzhili.spring.demo.dao.one;

import java.util.Date;
import lombok.Data;

/**
 * 
 * @since 1.0.0
 * @author jiangzhili
 * @date 2022/9/18
 */
@Data
public class MessageSendLog {
    private Long id;

    /**
    * 消息触达id
    */
    private String targetId;

    /**
    * 消息触达类型:10业务员,20商家

    */
    private Integer targetType;

    /**
    * 消息标题,增加可读性
    */
    private String title;

    /**
    * 消息内容
    */
    private String content;

    /**
    * 扩展字段
    */
    private String extendData;

    /**
    * 1成功,2失败
    */
    private Integer state;

    /**
    * 备注
    */
    private String remark;

    /**
    * saas模式下的租户id
    */
    private Long tenantId;

    /**
    * 拥有数据的组织结构的id
    */
    private Long ownerId;

    private Integer ownerType;

    /**
    * 是否已删除
    */
    private Boolean deleted;

    /**
    * 创建时间
    */
    private Date createTime;

    /**
    * 更新时间
    */
    private Date updateTime;

    /**
    * 创建人
    */
    private String createrId;

    /**
    * 修改人
    */
    private String updaterId;
}