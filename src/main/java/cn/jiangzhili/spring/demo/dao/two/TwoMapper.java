package cn.jiangzhili.spring.demo.dao.two;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 
 * @since 1.0.0
 * @author jiangzhili
 * @date 2022/9/18
 */
public interface TwoMapper extends BaseMapper<Two> {

}