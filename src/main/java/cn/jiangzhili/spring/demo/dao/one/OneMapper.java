package cn.jiangzhili.spring.demo.dao.one;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 
 * @since 1.0.0
 * @author jiangzhili
 * @date 2022/9/18
 */
public interface OneMapper extends BaseMapper<One> {

}