package cn.jiangzhili.spring.demo.dao.two;

import java.util.Date;
import lombok.Data;

/**
 * 
 * @since 1.0.0
 * @author jiangzhili
 * @date 2022/9/18
 */
/**
    * 通知表操作记录
    */
@Data
public class NoticeOperateLog {
    private Long id;

    /**
    * notice的id
    */
    private Long noticeId;

    /**
    * 上一个状态
    */
    private Integer lastState;

    /**
    * 当前状态
    */
    private Integer state;

    /**
    * saas模式下的租户id
    */
    private Long tenantId;

    /**
    * 拥有数据的组织机构的id
    */
    private Long ownerId;

    /**
    * 拥有数据的组织结构的类型
    */
    private Long ownerType;

    /**
    * 创建时间
    */
    private Date createTime;

    /**
    * 修改时间
    */
    private Date updateTime;

    /**
    * 创建人
    */
    private String createrId;

    /**
    * 修改人
    */
    private String updaterId;

    /**
    * 是否删除 0否 1是
    */
    private Boolean deleted;
}