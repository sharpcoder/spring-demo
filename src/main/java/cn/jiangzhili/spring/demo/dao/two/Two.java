package cn.jiangzhili.spring.demo.dao.two;

import lombok.Data;

/**
 * 
 * @since 1.0.0
 * @author jiangzhili
 * @date 2022/9/18
 */
@Data
public class Two {
    private Integer id;

    private String test;
}